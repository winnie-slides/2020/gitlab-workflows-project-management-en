---
title: Workflows and Project Management in GitLab
date: 2020-01-15
author:
  - Winnie Hellmann
  - "[winniehell.de](https://winniehell.de)"
title-slide-attributes:
  data-background-color: "#403368"
  data-background-image: img/gitlab-icon-rgb.png
  data-background-size: contain
---
 
# Who is Winnie? 🤔 {data-background-color="#403368"}

- 2013: started using GitLab
- 2015: introduced GitLab at previous company
- 2016: started contributing to GitLab
- 2017–2019: frontend engineer at GitLab
 
# What is this? 📋 {data-background-color="#403368"}

- task planning / requirement analysis (issues)
- estimating / scheduling (weights / milestones)
- workflows (labels / boards)
- code reviews (merge requests)

# GitLab basics 🔢 {data-background-color="#403368"}

- free software + enterprise features
- self-hosted or [gitlab.com](https://gitlab.com)
- started as web interface for Git
- now application for complete DevOps cycle

# Get started 🚀 {data-background-color="#403368"}

- choose or set up instance  
  (for example [gitlab.com](https://gitlab.com))
- [create account](https://docs.gitlab.com/ee/user/profile/account/create_accounts.html)
- [create group](https://docs.gitlab.com/ee/user/group/#create-a-new-group) (optional)
- create new project
  - [web interface](https://docs.gitlab.com/ee/gitlab-basics/create-project.html#create-a-project-in-gitlab)
  - [command line](https://docs.gitlab.com/ee/gitlab-basics/create-project.html#push-to-create-a-new-project)

# [Issues](https://docs.gitlab.com/ee/user/project/issues/) {data-background-image="img/paper-pile.jpg" data-background-opacity=0.75}

---

## Issues {data-background-image="img/paper-pile.jpg" data-background-opacity=0.25}

- describing tasks, bugs, or features ([labels](https://docs.gitlab.com/ee/user/project/labels.html))
- [commenting and discussing changes](https://docs.gitlab.com/ee/user/discussions/)
- [providing resources](https://docs.gitlab.com/ee/user/project/issues/design_management.html)
- [assigning work](https://docs.gitlab.com/ee/user/project/issues/issue_data_and_actions.html#3-assignee)
- [support requests](https://docs.gitlab.com/ee/user/project/service_desk.html)
- [time tracking](https://docs.gitlab.com/ee/user/project/time_tracking.html)

## Issues {data-background-image="img/paper-pile.jpg" data-background-opacity=0.25}

advanced features:

- [Quick Actions](https://docs.gitlab.com/ee/user/project/quick_actions.html)
- [Related Issues](https://docs.gitlab.com/ee/user/project/issues/related_issues.html)
- [CSV import](https://docs.gitlab.com/ee/user/project/issues/csv_import.html)
- [Description Templates](https://docs.gitlab.com/ee/user/project/description_templates.html)

# [Milestones](https://docs.gitlab.com/ee/user/project/milestones/) {data-background-image="img/milestone.jpg" data-background-opacity=0.65}

## Milestones {data-background-image="img/milestone.jpg" data-background-opacity=0.25}

- sprints or release cycles
- contain issues and merge requests
- allow filtering
- [burndown charts](https://docs.gitlab.com/ee/user/project/milestones/burndown_charts.html)


# [Issue Boards](https://docs.gitlab.com/ee/user/project/issue_board.html) {data-background-image="img/sticky-notes.jpg" data-background-opacity=0.5}

## Issue Boards {data-background-image="img/sticky-notes.jpg" data-background-opacity=0.15}

- overview of milestones, labels, teams
- [issue state transitions](https://docs.gitlab.com/ee/user/project/issue_board.html#creating-workflows)
- [prioritization](https://docs.gitlab.com/ee/user/project/issue_board.html#issue-ordering-in-a-list)
- scoping relevant issues

# [Merge Requests](https://docs.gitlab.com/ee/user/project/merge_requests.html) {data-background-image="img/waterfall.jpg" data-background-opacity=0.75}

## Merge Requests {data-background-image="img/waterfall.jpg" data-background-opacity=0.25}

- changes to common code base
- [code reviews](https://docs.gitlab.com/ee/user/discussions/#merge-request-reviews-premium)
- [approvals](https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html)
- [continuous integration](https://docs.gitlab.com/ee/ci/)

# Thank you for listening! {data-background-image="img/listening.jpg" data-background-opacity=0.5}

# Bonus slides ✨

## GitLab, the company

- founded 2011
- fully remote
- over [1100 team members](https://about.gitlab.com/company/team/)
- over [60 countries](https://about.gitlab.com/company/team/#countries) (50% from U.S.)
- fast growing
  - 2018: 200
  - 2019: 400

## Alternatives

- [GitHub](https://github.com/)
- [Bitbucket](https://bitbucket.org/)
- [Gitea](https://gitea.io/)
- [Space](https://www.jetbrains.com/space/)
