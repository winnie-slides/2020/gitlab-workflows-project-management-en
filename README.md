# Image sources

- [img/gitlab-icon-rgb.png](https://about.gitlab.com/press/press-kit/)
- [img/listening](https://unsplash.com/photos/Ez-XMWsYHsE)
- [img/milestone.jpg](https://unsplash.com/photos/Ww0NPSWYCGs)
- [img/paper-pile.jpg](https://unsplash.com/photos/MldQeWmF2_g)
- [img/sticky-notes.jpg](https://unsplash.com/photos/kvIAk3J_A1c)
- [img/waterfall.jpg](https://unsplash.com/photos/nwixMbLJy8E)
